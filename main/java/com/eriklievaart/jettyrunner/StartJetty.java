package com.eriklievaart.jettyrunner;

import java.io.File;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;

public class StartJetty {

	public static void main(String[] args) throws Exception {
		checkInput(args);

		Server server = new Server(8080);
		new StopJettyThread(server, 8079).start();

		addWar(args, server);

		server.start();
		server.join();
	}

	private static void checkInput(String[] args) {
		if (args.length != 1) {
			throw new Error("expecting exactly one war file as argument!");
		}
		File file = new File(args[0]);
		if (!file.isFile()) {
			throw new Error("Not a valid war file: " + args[0]);
		}
	}

	private static void addWar(String[] args, Server server) {
		WebAppContext webapp = new WebAppContext();
		webapp.setContextPath("/");
		webapp.setWar(args[0]);
		server.setHandler(webapp);
	}
}