package com.eriklievaart.jettyrunner;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import org.eclipse.jetty.server.Server;

class StopJettyThread extends Thread {

	private ServerSocket socket;
	private final Server server;

	public StopJettyThread(Server server, int port) {
		this.server = server;
		setDaemon(true);
		setName("StopMonitor");
		try {
			socket = new ServerSocket(port, 1, InetAddress.getByName("127.0.0.1"));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void run() {
		System.out.println("*** running jetty 'stop' thread");
		Socket accept;
		try {
			accept = socket.accept();
			BufferedReader reader = new BufferedReader(new InputStreamReader(accept.getInputStream()));
			reader.readLine();
			System.out.println("*** stopping jetty embedded server");
			server.stop();
			accept.close();
			socket.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			System.exit(0);
		}
	}
}